**Jiggle Web Pages**

Repository for original Jiggle web pages: https://pasadena.wr.usgs.gov/jiggle/

Upon commit, the .gitlab-ci.yml file also builds a copy of the documentation at https://aqms-swg.gitlab.io/jiggle-www/

(NOTE: Links to download jar files do not work on the GitLab site)

These web pages are located at `/webfiles/pasadena/jiggle/` on `agent86.gps.caltech.edu`
